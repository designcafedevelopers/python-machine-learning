#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Nov 10 21:29:54 2018

@author: derek
"""

# Used this tutorial: https://www.youtube.com/watch?v=tNa99PG8hR8
# Start from first video to get full understanding


import numpy as np
# Load iris data set
from sklearn.datasets import load_iris
from sklearn import tree
iris = load_iris()

# Testing data to be removed from the data and target variables of the data set
test_idx = [0, 50, 100]

# Training data
train_target = np.delete(iris.target, test_idx)
train_data = np.delete(iris.data, test_idx, axis=0)

# Testing data for checking accuracy of machine learning
test_target = iris.target[test_idx]
test_data = iris.data[test_idx]

# Train DecisionTreeClassifier using training data
clf = tree.DecisionTreeClassifier()
# Algorithm to find patterns in training data
clf.fit(train_data, train_target)

print (test_target)
print (clf.predict(test_data))

# Visualisation code
from sklearn.externals.six import StringIO
import pydotplus

# String buffer
dot_data = StringIO()
tree.export_graphviz(clf,
                     out_file=dot_data,
                     feature_names=iris.feature_names,
                     class_names=iris.target_names,
                     filled=True, rounded=True,
                     impurity=False)

graph = pydotplus.graph_from_dot_data(dot_data.getvalue())
# Generate PDF file displaying the decision tree
graph.write_pdf("iris.pdf")

print (test_data[0], test_target[0])